package de.sparfuchs.sparfuchsapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListView extends AppCompatActivity {
    public static android.widget.ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;

    @OnClick(R.id.button) void onCancelClick(){
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.button2) void onSparenClick(){
        //Add Data to Shopping List
        Intent i = new Intent(getApplicationContext(), Shopping.class);
        startActivity(i);
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);
        ButterKnife.bind(this);

        // Find the ListView resource.
        mainListView = (android.widget.ListView)findViewById( R.id.mainListView );

        // Create and populate a List of planet names.
        String[] planets = new String[] { "Eier", "Brot"};
        ArrayList<String> planetList = new ArrayList<String>();
        planetList.addAll( Arrays.asList(planets) );



        // Create ArrayAdapter using the planet list.
        listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, planetList);

        // Add more planets. If you passed a String[] instead of a List<String>
        // into the ArrayAdapter constructor, you must not add more items.
        // Otherwise an exception will occur.
        listAdapter.add( "Milch");
        listAdapter.add( "Käse");


        // Set the ArrayAdapter as the ListView's adapter.
        mainListView.setAdapter( listAdapter );
    }

}
